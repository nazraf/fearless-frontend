import React, {useEffect, useState} from 'react';

function ConferenceForm(props)  {
    const [locations, setLocations] = useState ([])

    const [name, setName] = useState('')
    const [starts, setStarts] = useState('')
    const [ends, setEnds] = useState('')
    const [description, setDescription] = useState('')
    const [max_presentations, setMax_presentations] = useState('')
    const [max_attendees, setMax_attendees] = useState('')
    const [location, setLocation] = useState('')

    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleStartChange = event => {
        const value = event.target.value
        setStarts(value)
    }

    const handleEndChange = event => {
        const value = event.target.value
        setEnds(value)
    }

    const handleDescriptionChange = event => {
        const value = event.target.value
        setDescription(value)
    }

    const handleMaxPresentationsChange = event => {
        const value = event.target.value
        setMax_presentations(value)
    }

    const handleMaxAttendeesChange = event => {
        const value = event.target.value
        setMax_attendees(value)
    }

    const handleLocationChange = event => {
        const value = event.target.value
        setLocation(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMax_presentations('');
        setMax_attendees('');
        setLocation('');
    }
}

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
    }
}

useEffect(()=> {
    fetchData();
}, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                className="form-control"
                onChange={handleNameChange}
                value={name}
                />
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Starts"
                required type="date"
                name="starts"
                id="starts"
                className="form-control"
                onChange={handleStartChange}
                value={starts}
                />
                <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Ends"
                required type="date"
                name="ends"
                id="ends"
                className="form-control"
                onChange={handleEndChange}
                value={ends}
                />
                <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
                {/* <!-- <input placeholder="Description" required type="text" name="description" id="description" className="form-control"> --> */}
                <label htmlFor="description">Description</label>
                <textarea
                className="form-control"
                name="description"
                id="description"
                rows="3"
                onChange={handleDescriptionChange}
                value={description}
                >
                </textarea>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Maximum presentations"
                required type="text"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
                onChange={handleMaxPresentationsChange}
                value={max_presentations}
                />
                <label htmlFor="maximum presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Maximum attendees"
                required type="text"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
                onChange={handleMaxAttendeesChange}
                value={max_attendees}
                />
                <label htmlFor="maximum attendees">Maximum Attendees</label>
            </div>
            <div className="mb-3">
                    <select
                    required name="location"
                    id="location"
                    className="form-select"
                    onChange={handleLocationChange}
                    value={location}
                    >
                    <option key="default" value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                            {location.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
